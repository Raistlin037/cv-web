let mouse = { x: -50, y: -50 }; // mouse pointer's coordinates
let noStars = 250; // number of stars
let maxSize = 5; // stars size
let starsDisplayed = false; // stars displayed flag
let parallaxInstance;
let currentSection = 'menu'; // the current section displayed
let sectionTransitionOn = false; // boolean that stores if a section transition is ongoing
let barsDisplayed = false; // bars displayed flag

$(document).ready( () => {
    
    // Go to top
    window.scrollTo({top:0, behavior: 'auto'});
    

    /**** CURSOR ****/
    let cursor = $('#cursor');
    
    cursorHovers(); // Get hoverable elements
      
    window.addEventListener('mousemove', updateCursor);

    $("body").mouseleave( () => {
        setTimeout( () => { cursor.css({ opacity: 0 }); }, 100);
    });


    /**** WELCOME ****/
    $("#welcome h2").bind('oanimationstart animationstart webkitAnimationStart', () => { 
        window.scrollTo({top:0, behavior: 'auto'}); // Go to top again (fix bug in server)
    });

    $("#enter").click( function() {
        if (starsDisplayed) hideStars();
        $(this).removeClass('opacity-0 fade-in clickable');
        $(this).addClass('fade-out'); // It doesn't trigger at correct moment
        $("#welcome h2").css({ opacity: 1, transform: 'translateY(-20%)' }).addClass('slide-down');
        setTimeout( () => { 
            $("#welcome").hide(); 
            cursor.removeClass('cursor-hover');
            $('body').css({ overflow: 'auto' });
        }, 2000);
        $('#song')[0].play();
        setTimeout( () => { 
            $('.aside-buttons').addClass('fade-in').css({right: '0'});
            $('#scroll-warning').css({ opacity: 1 });
            $('.home-name').addClass('fade-in').css({visibility: 'visible'});
        }, 1500);
    });


    /**** MENU ****/
    // Draw stars
    drawSatrs(noStars, maxSize);
    
    // Run Parallax
    let scene = $('#scene')[0];
    parallaxInstance = new Parallax(scene);

    // Menu buttons events
    $('.menu-button.menu').click( () => {
        $('#menu-links').addClass('visible');
    });
    $('.menu-button.close-menu').click( () => {
        $('#menu-links').removeClass('visible');
    });
    $('.menu-button.audio').click( () => {
        let audioIcon = $('.menu-button.audio i');
        if (audioIcon.hasClass('bi-volume-up')) {
            audioIcon.removeClass('bi-volume-up');
            audioIcon.addClass('bi-volume-mute');
            $('#song')[0].pause();
        } else {
            audioIcon.removeClass('bi-volume-mute');
            audioIcon.addClass('bi-volume-up');
            $('#song')[0].play();
        }
    });
    $('.menu-button.contact').click( () => {
        $('#modal-container').removeAttr('class').addClass('active');
    });
    $('.menu-button.pdf').click( () => { 
        window.open('./img/Javier Alcolea Ruiz.pdf', '_blank');
    });
    $('.menu-button.down').click( () => { 
        window.scrollTo({top: $('#scrollElement').height(), behavior: 'smooth'}); 
    });
    $('.menu-button.up').click( () => { 
        window.scrollTo({top:0, behavior: 'smooth'}); 
    });
    
    // Modal
    $('.link-contact').click( () => {
        $('#modal-container').removeAttr('class').addClass('active');
    });
    $('.modal-background').click( (e) => {
        (e.target == $('.modal-background')[0] || e.target.closest('.close-modal')) ? $('#modal-container').addClass('out') : e.stopPropagation();
    });


    // Menu-links effect
    let colorRays = $('.ray');
    let menu = $(".menu-links-wrap ul");
    let barNum = 7;
    let longestLink = $(".menu-links-wrap li:nth-child(1) span");
    colorRays.each(function(i) {
        $(this).attr({
            height: menu.height() / barNum,
            y: menu.height() / barNum * i
        });
    }); // set rays height
    $(".menu-links-wrap li span").each(function(i) { 
        if($(this).width() > longestLink.width()) {
            longestLink = $(this);
        }
    }); // Get longest span width of the list 

    $(".menu-links-wrap").mousemove(function(t) { // Adapted source from https://www.madwell.com/
        if (t.pageX < longestLink.offset().left + longestLink.width()) {
            var n = (t.pageY - $(this).offset().top) / $(this).outerHeight()
                , s = (t.pageX - $(this).offset().left) / $(this).outerWidth();
            d3.selectAll(".ray")
                .transition()
                .duration(100)
                .attr("width", function(t,i) {
                    return 10 + 240 * s * (1 - Math.abs(n - menu.height() / barNum * (i + .5) / menu.height()))
                })
                .attr("height", function(t,i) {
                    return menu.height() / (barNum + 2) * (.1 + Math.abs(n - menu.height() / barNum * (i + .5) / menu.height()))
                })
                .attr("y", function(t,i) {
                    return menu.height() / barNum * i + (menu.height() / barNum - menu.height() / (barNum + 2) * (.1 + Math.abs(n - menu.height() / barNum * (i + .5) / menu.height()))) / 2
                });
        } else {
            d3.selectAll(".ray").transition().duration(100)
                .attr("width", '10')
                .attr("height", menu.height() / barNum)
                .attr("y", function(t,i) {
                    return menu.height() / barNum * i
                });
        }
    }).mouseout(function() {
        d3.selectAll(".ray").transition().duration(300)
            .attr("width", '10')
            .attr("height", menu.height() / barNum)
            .attr("y", function(t,i) {
                return menu.height() / barNum * i
            });
    });

    // Menu button
    $('.link-home').click( () => {
        $('#menu-links').removeClass('visible');
        if(currentSection != 'menu' && !sectionTransitionOn) {
            // Section transitions
            currentSection = 'menu';
            sectionTransitionOn = true;
            $('section:not(#menu)').css({ opacity: 0});
            $('.home-name').css({ color: 'black', backgroundColor: 'transparent' });
            let promise = new Promise(function(resolve) { // Promise can be changed for two acumulative setTimeouts
                setTimeout(() => { 
                    $('#menu').css({ display: 'unset'});
                    window.scrollTo({top: localStorage.getItem("scrollPosition"), behavior: 'auto'});
                    resolve(); 
                }, 1000);
            });
            promise.then(
                () => { setTimeout(() => { 
                    $('#menu').css({ opacity: 1 });
                    //$('section:not(#menu)').css({ display: 'block'});
                    $('#studies .header-name').removeClass('fade-in-letters');
                    sectionTransitionOn = false;
                }, 1000); } // when the scroll is on top about section is shown
            );
        }
    });

    // Planet buttons events
    $('.link-studies').click( () => {
        $('#menu-links').removeClass('visible');
        if(currentSection != 'studies' && !sectionTransitionOn) {
            // Section transitions
            currentSection = 'studies';
            sectionTransitionOn = true;
            if (!starsDisplayed) showStars();
            $('section:not(#studies)').css({ opacity: 0});
            $('.home-name').css({ color: 'black', backgroundColor: 'transparent' });
            studiesImg.css({ transform: 'scale(5)' });
            let promise = new Promise(function(resolve) { // Promise can be changed for two acumulative setTimeouts
                setTimeout(() => { 
                    window.scrollTo({top:0, behavior: 'auto'});
                    $('section:not(#studies)').css({ display: 'none'});
                    $('#studies').css({ display: 'block'});
                    resolve(); 
                }, 1000);
            });
            promise.then(
                () => { setTimeout(() => { 
                    new Timeline('.timeline');
                    $('#studies').css({ opacity: 1 });
                    $('#studies .parallax-layer.name').addClass('fade-in-letters');
                    studiesImg.css({ transform: 'scale(1)' });
                    sectionTransitionOn = false;
                }, 750); } // when the scroll is on top about section is shown
            );
        }
    });

    $('.link-work').click( () => {
        $('#menu-links').removeClass('visible');
        if(currentSection != 'work' && !sectionTransitionOn) {
            // Section transitions
            currentSection = 'work';
            sectionTransitionOn = true;
            if (!starsDisplayed) showStars();
            $('section:not(#work)').css({ opacity: 0});
            $('.home-name').css({ color: 'white', backgroundColor: 'transparent' });
            workImg.css({ transform: 'scale(5)' });
            let promise = new Promise(function(resolve) { // Promise can be changed for two acumulative setTimeouts
                setTimeout(() => { 
                    window.scrollTo({top:0, behavior: 'auto'});
                    $('section:not(#work)').css({ display: 'none'});
                    $('#work').css({ display: 'block'});
                    resolve(); 
                }, 1000);
            });
            promise.then(
                () => { setTimeout(() => { 
                    $('#work').css({ opacity: 1 });
                    workImg.css({ transform: 'scale(1)' });
                    sectionTransitionOn = false;
                }, 750); } // when the scroll is on top work section is shown
            );
        }
    });

    $('.link-about').click( () => {
        $('#menu-links').removeClass('visible');
        if(currentSection != 'about' && !sectionTransitionOn) {
            // Section transitions
            currentSection = 'about';
            sectionTransitionOn = true;
            if (!starsDisplayed) showStars();
            $('section:not(#about)').css({ opacity: 0});
            $('.home-name').css({ color: 'white', backgroundColor: 'transparent' });
            aboutImg.css({ transform: 'scale(5)' });
            let promise = new Promise(function(resolve) { // Promise can be changed for two acumulative setTimeouts
                setTimeout(() => { 
                    window.scrollTo({top:0, behavior: 'auto'});
                    $('section:not(#about)').css({ display: 'none'});
                    $('#about').css({ display: 'block'});
                    resolve(); 
                }, 1000);
            });
            promise.then(
                () => { setTimeout(() => { 
                    $('#about').css({ opacity: 1 });
                    aboutImg.css({ transform: 'scale(1)' });
                    sectionTransitionOn = false;
                }, 750); } // when the scroll is on top about section is shown
            );
        }
    });

    $('.link-projects').click( () => {
        $('#menu-links').removeClass('visible');
        if(currentSection != 'projects' && !sectionTransitionOn) {
            // Section transitions
            currentSection = 'projects';
            sectionTransitionOn = true;
            if (!starsDisplayed) showStars();
            $('section:not(#projects)').css({ opacity: 0});
            $('.home-name').css({ color: 'white', backgroundColor: 'transparent' });
            $('#planet-projects').css({ 'overflow-x': 'unset' });
            projectsImg.css({ transform: 'scale(5)', 'z-index': 3 });
            let promise = new Promise(function(resolve) { // Promise can be changed for two acumulative setTimeouts
                setTimeout(() => { 
                    window.scrollTo({top:0, behavior: 'auto'});
                    $('section:not(#projects)').css({ display: 'none'});
                    $('#projects').css({ display: 'block'});
                    resolve(); 
                }, 1000);
            });
            promise.then(
                () => { setTimeout(() => { 
                    $('#projects').css({ opacity: 1 });
                    $('#planet-projects').css({ 'overflow-x': 'clip' });
                    projectsImg.css({ transform: 'scale(1)', 'z-index': 1 });
                    sectionTransitionOn = false;
                }, 1000); } // when the scroll is on top about section is shown
            );
        }
    });

    // Scroll animations
    let observerImg = $('#observer').children('img');
    let studiesImg = $('#planet-studies').children('img');
    let workImg = $('#planet-work').children('img');
    let aboutImg = $('#planet-about').children('img');
    let projectsImg = $('#planet-projects').children('img');
    let studiesBtn = $('#planet-studies').children('.menu-button');
    let workBtn = $('#planet-work').children('.menu-button');
    let aboutBtn = $('#planet-about').children('.menu-button');
    let projectsBtn = $('#planet-projects').children('.menu-button');

    window.addEventListener('scroll', () => {

        if(currentSection == 'menu') {

            // scrollY values from 0 to 5100
            let scrollY = window.scrollY;
            
            // Hide scroll warning
            (scrollY < 250) ? $('#scroll-warning').css({ opacity: 1 }) : $('#scroll-warning').css({ opacity: 0 });
            
            // Scene 1 - Little prince
            if (scrollY < 600) {
                observerImg.css({ top: scrollY/600 * -20 + '%', opacity: scrollY/600 });
                observerImg.css({ left: 0 + scrollY/5 + '%'});
            } else if (scrollY < 1000) {
                observerImg.css({ top: -20 * ((1000-scrollY)*2/1000) + '%', opacity: 1 });
                observerImg.css({ left: 20 * ((1000-scrollY)*2/100) + '%' });
            } else {
                observerImg.css({ top: '0', left: '0%', width: '100%', opacity: 1 });
            }
            
            // Scene 2 - Stars
            if (scrollY < 800) {
                observerImg.width(scrollY/8 + '%');
            } else {
                observerImg.width('100%');
                if (!starsDisplayed) showStars();
            }
            
            // Horizontal media
            if($(window).width() > $(window).height()) {

                // Scene 3 - Planet studies
                if (scrollY < 1500) {
                    studiesImg.width((scrollY-1000)/1000 * 25 + '%');
                    studiesImg.css({ top: (scrollY-1000)/15 + '%', opacity: (scrollY-1000)/500 });
                    studiesImg.css({ left: (scrollY-1000)/20 + '%' });
                    studiesBtn.css({ visibility: 'hidden' });
                } else if (scrollY < 2000) {
                    studiesImg.width((scrollY-1000)/1000 * 50 + '%');
                    studiesImg.css({ top: (scrollY-1000)/15 + '%', opacity: 1 });
                    studiesImg.css({ left: (scrollY-1000)/20 + '%' });
                    studiesBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else {
                    studiesImg.css({ top: '66%', left: '50%', width: '50%', opacity: 1 });
                    studiesBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }

                // Scene 4 - Planet work
                if (scrollY < 2400) {
                    workImg.css({ width: (scrollY-2000)/400 * 70 + '%', opacity: (scrollY-2000)/400 });
                    workImg.addClass('rotation');
                    workBtn.css({ visibility: 'hidden' });
                } else if (scrollY < 3000) {
                    workImg.css({ width: 45 + ((3000-scrollY)/600) * 25 + '%', opacity: 1 });
                    workImg.addClass('rotation');
                    workBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else {
                    workImg.css({  top: '35%', left: '40%', width: '45%', opacity: 1 });
                    workBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }
                
                // Scene 5 - Planet about
                if (scrollY < 3500) {
                    aboutImg.css({ width: (scrollY-3000)/500 * 45 + '%', opacity: (scrollY-3000)/500 });
                    aboutImg.addClass('ring');
                    aboutBtn.css({ visibility: 'hidden' });
                } else if (scrollY < 4000) {
                    aboutImg.css({ width: 45 + (scrollY-3000)/1000 * 25 + '%', opacity: 1 });
                    aboutImg.addClass('ring');
                    projectsImg.css({ opacity: (scrollY-4000)/500 });
                    $('#menguante').animate({backgroundColor: 'transparent'}, 500);
                    aboutBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else {
                    aboutImg.css({width: '70%', opacity: 1 });
                    aboutBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }
                
                // Scene 6 - Planet projects
                if (scrollY >= 4000 && scrollY < 4500) {
                    projectsImg.css({ opacity: (scrollY-4000)/500 });
                    $('#menguante').css({ left: ((4500-scrollY)/500 * 17.5 - 2.5) + '%', backgroundColor: '#FBFCFF' });
                    projectsBtn.css({ visibility: 'hidden' });
                } else if (scrollY >= 4500 && scrollY < 5000) {
                    $('#menguante').css({ left: ((5000-scrollY)/500 * 17.5 - 20) + '%', backgroundColor: '#FBFCFF' });
                    projectsBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else if (scrollY >= 5000) {
                    projectsImg.css({ opacity: 1 });
                    $('#menguante').css({ left: '-20%', backgroundColor: 'transparent' });;
                    projectsBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }
            } else { 
                // Vertical media

                // Scene 3 - Planet studies
                if (scrollY < 1500) {
                    studiesImg.width((scrollY-1000)/1000 * 20 + '%');
                    studiesImg.css({ top: (scrollY-1000)/30 + '%', opacity: (scrollY-1000)/500 });
                    studiesImg.css({ left: (scrollY-1000)/20 + '%' });
                    studiesBtn.css({ visibility: 'hidden' });
                } else if (scrollY < 2000) {
                    studiesImg.width((scrollY-1000)/1000 * 40 + '%');
                    studiesImg.css({ top: (scrollY-1000)/30 + '%', opacity: 1 });
                    studiesImg.css({ left: (scrollY-1000)/20 + '%' });
                    studiesBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else {
                    studiesImg.css({ top: '30%', left: '50%', width: '40%', opacity: 1 });
                    studiesBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }

                // Scene 4 - Planet work
                if (scrollY < 2400) {
                    workImg.css({ width: (scrollY-2000)/400 * 46 + '%', opacity: (scrollY-2000)/400 });
                    workImg.addClass('rotation');
                    workBtn.css({ visibility: 'hidden' });
                } else if (scrollY < 3000) {
                    workImg.css({ width: 30 + ((3000-scrollY)/600) * 25 + '%', opacity: 1 });
                    workImg.addClass('rotation');
                    workBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else {
                    workImg.css({  top: '15%', left: '10%', width: '30%', opacity: 1 });
                    workBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }
                
                // Scene 5 - Planet about
                if (scrollY < 3500) {
                    aboutImg.css({ width: (scrollY-3000)/500 * 32 + '%', opacity: (scrollY-3000)/500 });
                    aboutImg.addClass('ring');
                    aboutBtn.css({ visibility: 'hidden' });
                } else if (scrollY < 4000) {
                    aboutImg.css({ width: 32 + (scrollY-3000)/1000 * 18 + '%', opacity: 1 });
                    aboutImg.addClass('ring');
                    projectsImg.css({ opacity: (scrollY-4000)/500 });
                    $('#menguante').animate({backgroundColor: 'transparent'}, 500);
                    aboutBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else {
                    aboutImg.css({width: '50%', opacity: 1 });
                    aboutBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }
                
                // Scene 6 - Planet projects
                if (scrollY >= 4000 && scrollY < 4500) {
                    projectsImg.css({ opacity: (scrollY-4000)/500 });
                    $('#menguante').css({ left: ((4500-scrollY)/500 * 17.5 - 2.5) + '%', backgroundColor: '#FBFCFF' });
                    projectsBtn.css({ visibility: 'hidden' });
                } else if (scrollY >= 4500 && scrollY < 5000) {
                    $('#menguante').css({ left: ((5000-scrollY)/500 * 17.5 - 20) + '%', backgroundColor: '#FBFCFF' });
                    projectsBtn.css({ visibility: 'unset' }).removeClass('fade-in').addClass('fade-out');
                } else if (scrollY >= 5000) {
                    projectsImg.css({ opacity: 1 });
                    $('#menguante').css({ left: '-20%', backgroundColor: 'transparent' });;
                    projectsBtn.css({ visibility: 'unset' }).removeClass('fade-out').addClass('fade-in');
                }
            }
            
            // Save in local storage the scroll position
            localStorage.setItem("scrollPosition", scrollY);
        }

        // Home link color
        if(currentSection == 'studies' || currentSection == 'work' || currentSection == 'about' || currentSection == 'projects') {
            let parallaxHeight = $(`#${currentSection} .parallax`).height();
            if (scrollY < parallaxHeight * 0.95) {
                if(currentSection != 'studies') $('.home-name').css({ color: 'white' });
                if($(window).width() < 992) $('.home-name').css({ backgroundColor: 'transparent' });
            } else {
                if(currentSection != 'studies') $('.home-name').css({ color: 'black' });
                if($(window).width() < 992) $('.home-name').css({ backgroundColor: '#fffffff2' });
            }
        }

        // Control scroll position (debug mode)
        //console.log(scrollY);
    });

    workImg.bind('oanimationend animationend webkitAnimationEnd', () => { 
        workImg.removeClass('rotation'); // remove rotation class when animation ends
    });

    aboutImg.bind('oanimationend animationend webkitAnimationEnd', () => { 
        aboutImg.removeClass('ring'); // remove ring class when animation ends
    });


    /**** WORK ****/
    let workFullImg = $('#work .parallax-layer:nth-of-type(1)');
    let workMeImg = $('#work .parallax-layer:nth-of-type(2)');
    let workText = $('#work .parallax-layer:nth-of-type(3)');

    window.addEventListener('scroll', () => {
        if(currentSection == 'work') {
            // scrollY values from 0 to 3098
            let scrollY = window.scrollY;
            workFullImg.css({ transform: `translate3d(0px, ${scrollY * 1 + 'px'}, 0px)`});
            workText.css({ transform: `translate3d(0px, ${scrollY * 1.25 + 'px'}, 0px)`});
            workMeImg.css({ transform: `translate3d(0px, ${scrollY * 0.5 + 'px'}, 0px)`});

            if($(window).width() > 767) {
                $('.company-layer').each(function(i, v) {
                    let top = $(this).offset().top;
                    let windowHeight = $( window ).height();
            
                    if (scrollY + 30 > top) {
                        $(this).removeClass('move-left');
                    } else if (scrollY + windowHeight/2 > top) {
                        $(this).addClass('move-left');
                    } else {
                        $(this).removeClass('move-left');
                    }
                })
            }
        }
    });
    
    /**** ABOUT ****/
    let aboutFullImg = $('#about .parallax-layer:nth-of-type(1)');
    let aboutMountainImg = $('#about .parallax-layer:nth-of-type(2)');
    let aboutMeImg = $('#about .parallax-layer:nth-of-type(3)');
    let aboutText = $('#about .parallax-layer:nth-of-type(4)');

    window.addEventListener('scroll', () => {
        if(currentSection == 'about') {
            // scrollY values from 0 to 3351
            let scrollY = window.scrollY;
            aboutFullImg.css({ transform: `translate3d(0px, ${scrollY * 1 + 'px'}, 0px)`});
            aboutText.css({ transform: `translate3d(0px, ${scrollY * 1.25 + 'px'}, 0px)`});
            aboutMountainImg.css({ transform: `translate3d(0px, ${scrollY * 0.5 + 'px'}, 0px)`});
            aboutMeImg.css({ transform: `translate3d(0px, ${scrollY * 0.25 + 'px'}, 0px)`});
        }
    });
    

    /**** PROJECTS ****/
    let projectsName = $('#projects .parallax-layer:nth-of-type(1)');
    //let projectsText = $('#projects .parallax-layer:nth-of-type(2)'); // projectsText placed in the bottom at latest versions

    window.addEventListener('scroll', () => {
        if(currentSection == 'projects') {
            // scrollY values from 0 to 1632
            let scrollY = window.scrollY;
            projectsName.css({ transform: `translate3d(${scrollY * -1.1 + 'px'}, 0px, 0px) scale(${1 + scrollY * 0.001})` });
            //projectsText.css({ transform: `translate3d(${scrollY * -1.25 + 'px'}, 0px, 0px) scale(${1 + scrollY * 0.001})` });
            projectsName.css({ opacity: 1 - (scrollY/1000) });
            //projectsText.css({ opacity: 1 - (scrollY/1000) });
        }
    });
});

/**** ADDITIONAL FUNCTIONS ****/
// Stars functions
function drawSatrs(noStars, maxSize) {
    let sky = $('#sky');
    let stars = {};
    
    for (i = 0; i <= noStars; i++) {
        let star = $('<span>', { class: 'star' });
        let size = randomSize(maxSize);
        star.addClass('size-' + size);
        star.css({
            height: ( $(window).width() > 767 ? size : size/2 ) + 'px',
            width: ( $(window).width() > 767 ? size : size/2 ) + 'px',
            opacity: 0,
            left: $('#sky').width()/2 + 'px',
            top: $('#sky').height()/2 + 'px'
        });
        
        sky.append(star);
    }
    
    for (i = 1; i <= maxSize; i++) {
        stars[i] = $('.size-' + i);
    }

    $('#menu').mousemove( (event) => {
        let objLeft = $('#sky').offset().left;
        let objTop = $('#sky').offset().top;
      
        let objCenterX = objLeft + $('#sky').width() / 2;
        let objCenterY = objTop + $('#sky').height() / 2;
      
        let x = invertNumber(Math.floor(event.pageX - objCenterX));
        let y = invertNumber(Math.floor(event.pageY - objCenterY));
        
        for (i = 1; i <= maxSize; i++) {
            stars[i].velocity(
                { translateX: x * (i / 300) + 'px', translateY: y * (i / 150) + 'px' },
                { duration: 5 }
            );
        }
    });
}

function showStars() {
    $('span.star').each( (i, el) => {
        let position = randomPosition();
        $(el).css({
            opacity: 1,
            left: position.left + 'px',
            top: position.top + 'px'
        });
    });
    starsDisplayed = true;
}

function hideStars() {
    $('span.star').css({
        opacity: 0,
        left: $('#sky').width()/2 + 'px',
        top: $('#sky').height()/2 + 'px'
    });
    starsDisplayed = false;
}

function randomSize(maxSize) {
    return Math.floor(Math.random() * maxSize + 1);
}

function randomPosition() {
    let coordinates = {};
    coordinates.left = Math.floor(Math.random() * $('#sky').width() + 1);
    coordinates.top = Math.floor(Math.random() * $('#sky').height() + 1);
    
    return coordinates;
}

function invertNumber(number) {
    if(number < 0) {
      return Math.abs(number)
    } else {
      return -Math.abs(number)
    }
}

// Cursor
// This functions adds mouseover and mouseleave events to several elements to trigger cursor animation.
function cursorHovers() {
    setTimeout(() => {
        let cursor = $('#cursor');
        let clickable = $('.clickable');
        clickable.mouseenter( () => {
            cursor.addClass('cursor-hover');
        });
        clickable.mouseleave( () => {
            cursor.removeClass('cursor-hover');
        });
    }, 500);
}
  
function updateCursor(e) {
    let cursor = $('#cursor');
    // Update coordinates
    mouse.x = e.clientX;
    mouse.y = e.clientY;
    
    // Update cursor
    cursor.css({ top: mouse.y - 7.5 + 'px', left: mouse.x - 10 + 'px'});
    if (mouse.y < 5 || mouse.x < 5 || mouse.x + 5 > $(window).width() || mouse.y + 5 > $(window).height()) {
        cursor.css({ opacity: 0 });
    } else {
        cursor.css({ opacity: 1 });
    }
};


// Bars
function startBars() {
    barsDisplayed = true;
	$('.bar').each(function (i) {
		var $bar = $(this);
		$(this).append('<span class="count"></span>')
		setTimeout(function () {
			$bar.css('width', $bar.attr('data-percent'));
		}, i * 100);
	});
	$('.count').each(function () {
		$(this).prop('Counter', 0).animate({
			Counter: $(this).parent('.bar').attr('data-percent')
		}, {
			duration: 2000,
			easing: 'swing',
			step: function (now) {
				$(this).text(Math.ceil(now) + '%');
			}
		});
	});
};

function hideBars() {
    $('.bar').css('width', 0);
    $('.count').remove();
    barsDisplayed = false;
}


// Timeline
// Source: https://codepen.io/accudio/pen/YzzyMxN
function Timeline(selector, config) {
    this.el = $(selector);
    isHorizontal = ($(window).width() > $(window).height()) ? true : false
    
    const defaults = {
        track: {
            endAtLast: false
        },
        viewPointBottom: false,
        viewPoint: isHorizontal ? $('.timeline').offset().top * 0.68 : ($('.timeline').offset().top * 0.75)
    }
    this.options = $.extend({}, defaults, config || {})
    
    $(document).ready(() => {
        this.init()
    })
    
    this.init = function() {
        this.el.addClass('is-loading')
        this.el.addClass('is-init')
        this.el.each(function() {this.offsetHeight})
        this.el.removeClass('is-loading')
        this.animation()
        this.trackHeight()
        
        let self = this
        $(document).scroll(function() {
            self.animation()
        })
        $(document).resize(function() {
            self.trackHeight()
        })
    }
    
    this.animation = function() {
        let self = this
        
        let scrollTop = $(document).scrollTop()
        let viewPoint = isHorizontal ? scrollTop + this.options.viewPoint : (scrollTop + this.options.viewPoint) * 1.3
        if (this.options.viewPointBottom) {
            viewPoint = scrollTop + window.innerHeight - this.options.viewPoint
        }
    
        this.updateTrack(viewPoint)
    
        $('.timeline__item', this.el).each(function(i, v) {
            let top = $(this).offset().top
            let bottom = $(this).offset().top + $(this).outerHeight(true)
    
            if (viewPoint < top) {
                self.updateClasses(this, 'is-below')
            } else if (viewPoint > bottom) {
                self.updateClasses(this, 'is-above is-visible')
            } else {
                self.updateClasses(this, 'is-current is-visible')
            }
        })
        
        if ($('.timeline__footer').length) {
            let footer = '.timeline__footer'
            let top = $(footer).offset().top
    
            if (viewPoint < top) {
                self.updateClasses(footer, '')
            } else {
                self.updateClasses(footer, 'is-visible')
            }
        }
        
        if ($('.bar-wrapper').length) {
            let barWrapepr = '.bar-wrapper'
            let top = $(barWrapepr).offset().top

            if (viewPoint < top) {
                $('.bar-wrapper').css({ opacity: 0 })
                if (barsDisplayed) hideBars()
            } else {
                $('.bar-wrapper').css({ opacity: 1 })
                if (!barsDisplayed) startBars()
            }
        }
    }
        
    this.updateClasses = function(el, newClass) {
        $(el).removeClass('is-above is-current is-below is-visible')
        $(el).addClass(newClass)
    }
    
    this.updateTrack = function(viewPoint) {
        $el = $('.timeline__track', '.timeline')
        let top = $el.offset().top
        let height = viewPoint - top
        $el.height(height)
    }
    
    this.trackHeight = function() {
        let trackMax =  this.el.outerHeight() + 25
        if ($('.timeline__footer').length) {
            trackMax -= $('.timeline__footer').outerHeight()
        }
        if (this.options.track.endAtLast) {
            trackMax = trackMax - $('.timeline__item').last().outerHeight() + 9
        }
        $('.timeline__track', this.el).css('max-height', trackMax)
    }
}
